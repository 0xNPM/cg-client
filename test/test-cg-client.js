/**
 *
 * @module test-cg-client
 * @author H Svalin (PE)
 */
"use strict";

const
    assert = require('chai').assert,
    winston = require('winston'),
    CGClient = require('../lib/cg-client');

const config = require('./config.json');

/**
 * Create a logger for testing purposes...
 */
function createTestLogger(name, level){
    name = name || 'TestCase-Logger';
    level = level || 'debug';

    let result = new (winston.Logger)({
        name: name,
        level: level,
        label: 'testing',
        transports: [
            new (winston.transports.Console)()
        ]
    });
    return result;
}


describe("CG Client initial setup and user details test", function(){
    this.timeout(5000);

    let customerNumber = "87";
    let networkId = "20";
    let logger = createTestLogger();

    it("should create a client instance and get customer", function(done) {
        let opts = {
            customerNumber: customerNumber,
            netId: networkId,
            cg: config
        };
        let client = new CGClient(opts, logger);

        client.refresh()
            .then(result => {
                assert.isBoolean(result);
                assert(result);
                done();
            })
            .catch(err => {
                done(err);
            });
    });

    it("there should maybe be some cards associated with the a customer", function(done) {
        let opts = {
                customerNumber: customerNumber,
                netId: networkId,
                cg: config
            },
            client = new CGClient(opts, createTestLogger());

        client.cards()
            .then(result => {
                assert.isArray(result);

                done();
            })
            .catch(err => {
                done(err);
            });
    });

    it("should be possible to get customer details", function(done) {
        let opts = {
                customerNumber: customerNumber,
                netId: networkId,
                cg: config
            },
            client = new CGClient(opts, createTestLogger());

        client.details()
            .then(result => {
                assert.isObject(result);
                assert(result.customerNumber === opts.customerNumber);

                done();
            })
            .catch(err => {
                done(err);
            });
    });
});