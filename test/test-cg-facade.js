/**
 * File:
 *     test-cg-facade.js
 *
 * Description:
 *
 *      Test the CG Facade utility.
 *
 * Created by:
 *      hakansvalin on 2017-02-20.
 */
"use strict";
const assert = require('chai').assert;
const cg_facade_factory = require('../index').cgFacadeFactory;

const config = require('./config.json');

describe("Take CG facade through its pings", function() {
    let cg = cg_facade_factory(Object.assign({}, config, {trace: false}));

    it("should be pingable w callback", function(done) {
        this.timeout(5000);

        cg.ping((error, response)=>{
            if ( error ) {
                return done(error);
            }
            assert.isString(response);
            done();
        });
    });

    it("should be pingable as promise", function (done) {
        this.timeout(5000);

        cg.pingPromise().then(version => {
            assert.isString(version);
            console.log("Version information (promise): ", version);
            done();
        }).catch(err => {
            done(err);
        });
    });

});

describe("Call CGs operations, with tracing on", function() {

    let cg = cg_facade_factory(Object.assign({}, config, {trace: true}));

    it("should list some networks using callback based operation call", function(done){
        this.timeout(2000);

        cg.callOperation('listnetworks', (error, response, parsedResponse, trace) => {
            if ( error ) {
                return done(error);
            } else {
                assert.isObject(response);
                assert.property(response, 'statusCode');

                assert.isObject(parsedResponse);
                assert.property(parsedResponse, 'NetworkList');
                assert.isObject(parsedResponse.NetworkList);
                assert.property(parsedResponse.NetworkList, 'Network');
                assert.isArray(parsedResponse.NetworkList.Network);

                assert.isObject(trace);
                assert.property(trace, 'in');
                assert.property(trace, 'out');

                done();
            }
        });
    });

    it("should list some networks using promised based operation call", function(done){
        this.timeout(2000);
        cg.callOperationPromise("listnetworks").then(results => {

            assert.isArray(results);
            assert(results.length === 3);

            assert.isObject(results[0]); // the raw response
            assert.isObject(results[1]); // the parsedRespons (body)
            assert.isObject(results[2]); // the trace object

            done();
        }).catch(err => {
            done(err);
        });
    });

    it("should timeout if the there is a short enough timeout", function(done) {

        cg.callOperationPromise("listnetworks", null, 0.001).then(results => {
            done("Not what was expected");
        }).catch(err => {
            console.log("Good, this is what I want:", err.toString());
            done();
        });
    });

});