/**
 * File:
 *     test-cg-operations.js
 *
 * Description:
 *
 *      Test the cg-operations module, i.e. the CGOperation class.
 *
 * Created by:
 *
 *      hakansvalin on 2017-03-26.
 */
"use strict";

const assert = require('chai').assert,
    ASQ = require('asynquence-contrib');

const options = require('./config.json');
const CGOperations = require('../index').CGOperations;

describe("Customer related operations, i.e. creating, (NOT) updating, finding and deleting", function () {
    const netId = "20",
        fullCustomer = {
            customerNumber: "VE-11001",
            name: "Nomen Nescius",
            sseqNo: "195812281212",
            address: {
                co: "Gary Glitter",
                street: "Oneway road 12",
                postalCode: "123 45",
                city: "Ghosttown"

            },
            contact: {
                phone: "+46 8 12345678",
                mobile: "+46 71 1010101",
                email: "nomen.nescius@nodomain.com"
            },
            invoiceAddress: {
                name: "Gary Glitter",
                co: "Gold Daddy",
                street: "Gateway 10",
                postalCode: "ZIP 12345",
                city: "The Town"
            }
        };

    let cgo = new CGOperations(options);

    it("should not exist a customer with customer id: " + fullCustomer.customerNumber, function(done){
        this.timeout(5000);

        cgo.customerExists(fullCustomer.customerNumber).then(result => {
            assert.isBoolean(result);
            assert(result === false, "Customer exists");
            done();
        }).catch(err => {
            done(err);
        });
    });


    it("should be able to add a customer with a complete set of properties", function(done) {
        this.timeout(5000);

        cgo.addCustomer(fullCustomer)
            .then(result => {
                assert.isObject(result);
                assert(result.customerNumber === fullCustomer.customerNumber, "Not the customer number expected");
                assert(result.netId === netId);
                done();
            })
            .catch(err => {
                done(err);
            });
    });

    it("should be possible to retrieve the customer added", function(done) {
        this.timeout(5000);

        cgo.getCustomer(fullCustomer.customerNumber)
            .then(result => {
                let referenceCustomer = Object.assign({}, fullCustomer, {netId: options.netid});

                assert.isObject(result);
                assert.deepEqual(result, referenceCustomer, "Bad full customer");
                done();
            })
            .catch(err => {
                done(err);
            });
    });

    it("should be possible to delete the customer as well", function (done) {
        this.timeout(60000);

        cgo.deleteCustomer(fullCustomer.customerNumber)
            .then(result => {
                assert.isBoolean(result);
                assert(true === result, "Failed to delete customer");
                done();
            })
            .catch(err => {
                done(err);
            });
    });

});

describe("Card related operations, e.g. adding, getting, revoking cards", function () {

    let cgo = new CGOperations(options);
    let minimalCustomer = {
        customerNumber: "VE-11002",
        name: "Alfred E Neuman"
    };

    let cardNumber = "00041801735111"; // A card number that works...
    let twinCardNumber = "00041801813116"; //"00041801783112"; //"00041801771119";

    let storedCard = "00041801340117";

    before(function(done){
        cgo.addCustomer(minimalCustomer)
            .then(result => {
                done();
            }).catch(err => {
            done(err);
        });
    });

    after(function(done){
        this.timeout(60000);
        cgo.deleteCustomer(minimalCustomer.customerNumber)
            .then(result => {
                done();
            }).catch(err => {
                done(err);
        });
    });

    it("should be possible to get details of a card", function(done) {
        this.timeout(5000);
        let specialCardnumber = "00430002402104"; // Card number found in CGs demo server

        cgo.listCard(specialCardnumber).then(result => {
            assert.isObject(result);

            // TODO extend test
            assert.property(result, 'number');

            done();
        }).catch(err => {
            done(err);
        })
    });

    it("should be possible to get a customers all cards", function (done) {
        this.timeout(5000);
        let specialCustomerNumber = "25"; // Customer with multiple cards found in CGs demo server
        let excludeStored = false, networkId = 21;

        cgo.listCustomerCards(specialCustomerNumber, excludeStored, networkId).then(result => {
            assert.isArray(result);
            assert(result.length > 0 );
            done();
        }).catch(err => {
            done(err);
        });
    });

    it("add card to customer", function(done) {
        this.timeout(60000);

        let irdNumber = "00060003011";

        cgo.addCard(minimalCustomer.customerNumber, cardNumber, {irdNumber: irdNumber}).then(result => {
            assert.isBoolean(result);
            assert(result);

            done();
        }).catch(err => {
            done(err);
        });
    });

    it("should be possible to pair a card to a device", function(done) {
        this.timeout(5000);

        let irdNumber = "00060003010";

        cgo.pairCard(cardNumber, {irdNumber: irdNumber})
            .then(result => {
                assert.isBoolean(result);
                assert(result);
                done();
            })
            .catch(err => {
                done(err);
            });
    });

    it("should be possible to unpiar a card from a device", function(done) {
        this.timeout(5000);

        cgo.unpairCard(cardNumber)
            .then(result => {
                assert.isBoolean(result);
                assert(result);
                done();
            })
            .catch(err => {
                done(err);
            })
    });

    it("add extra/twin card to customer", function(done) {
        this.timeout(5000);

        cgo.addCard(minimalCustomer.customerNumber, twinCardNumber).then(result => {
            assert.isBoolean(result);
            assert(result);

            done();
        }).catch(err => {
            done(err);
        });
    });

    it("should be possible to connect a card", function(done){
        cgo.connectCard(cardNumber, twinCardNumber)
            .then(result=>{
                assert.isBoolean(result);
                assert(result);

                done();
            })
            .catch(err=>{
                done(err);
            });
    });

    it("should be possible to invalidate card", function(done) {
        cgo.invalidateCard(cardNumber).then(result => {
            assert.isBoolean(result);
            assert(result);

            done();
        }).catch(err => {
            done(err);
        });
    });

    it("should be possible to revalidate card", function(done) {
        cgo.revalidateCard(cardNumber)
            .then(result => {
                assert.isBoolean(result);
                assert(result);

                done();
            })
            .catch(err => {
                done(err);
            });
    });

    it("should be possible to deconnect a card", function (done){
        cgo.deconnectCard(twinCardNumber)
            .then(result=>{
                assert.isBoolean(result);
                assert(result);

                done();
            })
            .catch(err=>{
                done(err);
            });
    });

    it("store cards", function(done) {
        cgo.blacklistCard(twinCardNumber).then(result => {
            return cgo.blacklistCard(cardNumber);
        }).then(result => {
            assert.isBoolean(result);
            assert(result);

            done();
        }).catch(err => {
            done(err);
        });
    });

});

describe("Exchange cards, does that thing work", function() {

    this.timeout(5000);

    let cgo = new CGOperations(options);
    let minimalCustomer = {
        customerNumber: "VE-11003",
        name: "Donald A Trumpet"
    };

    let cardNumber = "00041801735111";
    let articleNumber = "9902";

    let newCardNumber = "00041801813116";

    let subsOldCard = [];

    before(function(done) {
        cgo.addCustomer(minimalCustomer)
            .then(result => {
                return cgo.addCard(minimalCustomer.customerNumber, cardNumber);
            })
            .then(result => {
                return cgo.addSubscription(cardNumber, articleNumber);
            })
            .then(result => {
                return cgo.listSubscriptions(cardNumber);
            })
            .then(result => {
                subsOldCard = result;
                done();
            })
            .catch(err => {
                done(err);
            });
    });

    after(function(done) {
        this.timeout(60000);
        cgo.deleteCustomer(minimalCustomer.customerNumber)
            .then(result => {
                done();
            }).catch(err => {
            done(err);
        });
    });

    it("should be possible to exchange cards", function(done) {

        cgo.exchangeCard(cardNumber, newCardNumber)
            .then(result => {
                assert.isBoolean(result);
                assert(true===result);
                done();
            })
            .catch(err => {
                done(err);
            });
    });

    it("is the postcondition after the exchange correct", function(done){

        Promise
            .all([
                cgo.listCard(cardNumber),
                cgo.listCard(newCardNumber),
                cgo.listSubscriptions(newCardNumber)
            ])
            .then(results => {
                let [oldCard, newCard, subsNewCard] = results;

                assert(oldCard.isStored === true);
                assert(newCard.isStored === false);

                assert.deepEqual(subsNewCard, subsOldCard, "subscriptions mismatch");

                done();
            })
            .catch(err => {
                done(err);
            });
    });

});

describe("Subscription related operations, adding, stopping", function() {
    this.timeout(5000);

    let cgo = new CGOperations(options);
    let minimalCustomer = {
        customerNumber: "VE-11002",
        name: "Alfred E Neuman"
    };

    let cardNumber = "00041801735111"; // A card number that works...
    let articleNumber = "30";
    let xArticleNumber = "9902";

    before(function(done){
        cgo.addCustomer(minimalCustomer)
            .then(result => {
                return cgo.addCard(minimalCustomer.customerNumber, cardNumber);
            })
            .then(result => {
                done();
            })
            .catch(err => {
                done(err);
            });
    });

    after(function(done){
        this.timeout(60000);
        cgo.deleteCustomer(minimalCustomer.customerNumber)
            .then(result => {
                done();
            })
            .catch(err => {
                done(err);
            });
    });

    it("should be possible to list subscriptions for a known card", function(done) {
        cgo.listSubscriptions(cardNumber)
            .then(result => {
                assert.isArray(result);
                done();
            })
            .catch(err => {
                done(err);
            });
    });

    it("should be possible to add a subscription", function(done){

        cgo.addSubscription(cardNumber, articleNumber)
            .then(result => {
                assert.isBoolean(result);
                assert(result);

                done();
            })
            .catch(err => {
                done(err);
            });
    });

    it("should be possible to stop a subscription", function(done){

        cgo.stopSubscription(cardNumber, articleNumber)
            .then(result => {
                assert.isBoolean(result);
                assert(result);

                done();
            })
            .catch(err => {
                done(err);
            });
    });

    it("should be possible to add a subscription with start and end date as strings", function(done) {

        cgo.addSubscription(cardNumber, xArticleNumber, "2017-05-01", "2017-07-30")
            .then(result => {
                assert.isBoolean(result);
                assert(result);

                done();
            })
            .catch(err => {
                done(err);
            });
    });

    it("should be possible to end a subscription with a given stop date", function (done) {

        cgo.stopSubscription(cardNumber, xArticleNumber, new Date("2017-05-01"))
            .then(result => {
                assert.isBoolean(result);
                assert(result);

                done();
            })
            .catch(err => {
                done(err);
            });

    });
});