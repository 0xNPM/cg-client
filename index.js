/**
 * <strong>CG-Client</strong><br><br>
 *
 * @module cg-client
 * @author HSvalin (PE)
 */
"use strict";

// Module version information
const VERSION_INFORMATION = "0.0.3";

/**
 * <strong>Root Module</strong><br><br>
 *
 * @type {{CGOperations: CGOperations, cgFacadeFactory: cryptoguard_facade_factory}}
 */
module.exports = {
    CGCustomerClient: require('./lib/cg-client'),
    CGOperations: require('./lib/cg-operations'),
    cgFacadeFactory: require('./lib/cg-facade')
};