/**
 * File:
 *
 *     utils.js
 *
 * Description:
 *
 *      Commonly used functions
 *
 * Created by:
 *
 *      hakansvalin on 2017-02-21.
 */
"use strict";

const   w = require('winston');

// Module version number/information string
const VERSION_INFORMATION = "0.0.4";

/**
 * <strong>Create a default Winston logger</strong>
 * <br><br>
 *
 * Creates and returns a default Winston logger to use when no other is specified.
 *
 * The logger will by default use a <code>Console</code> transport.
 *
 * @param {string} name - Name of the logger
 * @param {string} [level=debug] - The logger level
 */
function create_default_logger(name, level) {
    name = name || 'Default Logger';
    level = level || 'debug';

    let result = new (w.Logger)({
        name: name,
        level: level,
        transports: [
            new (w.transports.Console)()
        ]
    });
    return result;
}

/**
 * Merge the defaults with the specified opts.
 *
 * This is probably something that can be done by other means, e.g.
 * some nice 3:rd party module...
 * @param opts
 * @param defaults
 * @returns {*}
 */
let _mergeOptions = function (opts, defaults) {
    for(let name in defaults) {
        if (opts.hasOwnProperty(name)) {
            if( typeof opts[name] === 'object' ) {
                _mergeOptions(opts[name], defaults[name]);
            }
        } else {
            opts[name] = defaults[name]
        }
    }
    return opts;
};

module.exports = {
    mergeOptions: _mergeOptions,
    createDefaultLogger: create_default_logger
};