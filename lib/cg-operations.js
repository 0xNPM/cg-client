/**
 * <strong>CG-Operations module</strong><br><br>.
 *
 * @module cg-operations
 * @author HSvalin (PE)
 */
"use strict";
const
    _ = require('lodash'),
    moment = require('moment');

// Local dependencies
const sv_utils = require('./utils');
const cg_facade_factory = require("./cg-facade");

// Attribute map for customer info to CG customer conversion. Used when creating/adding customers
const CUSTOMER_ATTRIBUTE_MAP = {
    customerNumber: "customernumber",
    name: "customername",
    sseqNo: "customerssnumber",
    address: {
        co: "customercoaddress",
        street: "customeraddress",
        postalCode: "customerpostcode",
        city: "customercity"
    },
    contact: {
        phone: "customerphone",
        mobile: "customermobilephone",
        email: "customeremail"
    },
    invoiceAddress: {
        name: "facturename",
        co: "facturecoaddress",
        street: "factureaddress",
        postalCode: "facturepostcode",
        city: "facturecity"
    }
};

// Module version number/information string
const VERSION_INFORMATION = "0.0.4";

// CG customer attribute to CGOperations customer map
const CG_CUSTOMER_TO_CUSTOMER = [
    ["CustomerNumber", "customerNumber"],
    ["NetID", "netId"],
    ["CustomerName", "name"],
    ["CustomerSSNumber", "sseqNo"],
    ["CustomerCoAddress", "address.co"],
    ["CustomerAddress", "address.street"],
    ["CustomerPostCode", "address.postalCode"],
    ["CustomerCity", "address.city"],
    ["CustomerPhone", "contact.phone"],
    ["CustomerMobilePhone", "contact.mobile"],
    ["CustomerEmail", "contact.email"],
    ["FactureName","invoiceAddress.name"],
    ["FactureCoAddress", "invoiceAddress.co"],
    ["FactureAddress", "invoiceAddress.street"],
    ["FacturePostCode", "invoiceAddress.postalCode"],
    ["FactureCity", "invoiceAddress.city"]
];
const CG_CUSTOMER_TO_CUSTOMER_MAP = new Map(CG_CUSTOMER_TO_CUSTOMER);

/**
 * Map attributes from the source object to the target object according
 * to the specification object.
 *
 * @param {Object} source - Source object
 * @param {Object} target - Target object
 * @param {Object} mapSpec - Mapping specification object
 * @private
 */
function mapAttributes(target, source, mapSpec) {

    for ( let sname in mapSpec ) {
        if ( sname in source ) {
            if (_.isObject(source[sname]) ) {
                mapAttributes(target, source[sname], mapSpec[sname]);
            } else {
                target[mapSpec[sname]] = source[sname];
            }
        }
    }
    return target;
}

/**
 * <strong>CG error object</strong>
 * <br><br>
 *
 * This is a prototype descendant of Error and adds the extra proerties:
 *
 *  <pre>
 *      name    {string}    Name of error, e.g CGError
 *      message {String}    Error message and error number from CG, e.g:
 *          The cardnumber is invalid. CRC failure [3]
 *
 *      cgError {Object}    The parsed CG error returned from the normalizeError function
 *          defined on the CG-Facade object.
 * </pre>
 *
 * @param {Object} cgError - A normalized CG error response, see CG-Facade and normalizeError
 * @constructor
 */
function CGError(cgError) {
    this.name = "CGError";
    this.message = `${cgError.errorMessage} [${cgError.errorNumber}]`;
    this.cgError = cgError;
    this.stack = (new Error()).stack;
};

CGError.prototype = Object.create(Error.prototype);
CGError.prototype.constructor = CGError.constructor;

/**
 * Construct a CGOperations object that will use the specified cryptoguard API
 * facade.<br><br>
 *
 *
 * @param {Object} options - Configuration options for this CG Operations
 * @param {String} options.baseUrl - Base URL for the Cryptoguard API server, e.g http://demo.cryptoguard.se
 * @param {String} options.token - Cryptoguard API access token
 * @param {String|Number} options.netid - The network id
 *
 * @param {Object} logger - Optional (Winston) logger object. If not specified a default logger
 *  will be created and this will be logging to the console.
 *
 * @constructor
 */
function CGOperations(options, logger) {

    this._cg_facade = cg_facade_factory(options, logger);

    if ( options.netid ) {
        this._netid = options.netid;
    } else {
        throw new Error("Net ID not specified");
    }

    if ( !logger ) {
        this._logger = sv_utils.createDefaultLogger('CGOperations');
    } else {
        this._logger = logger;
    }
}

/**
 * Return the configured Winston logger.
 * @returns {*|Object}
 */
CGOperations.prototype.logger = function() {
    return this._logger;
};

/**
 * Returns a reference to the CryptoGuard Facade object in use.
 * @returns {{version: *, options: *}|*}
 */
CGOperations.prototype.cg = function() {
    return this._cg_facade;
};


/**
 * Generic operation to execute a CG operations that results in either an
 * OK or ERROR result, i.e. bearing no other information in the response.
 *
 * This method is for internal use mostly...
 *
 * @param {String} name - Name of operation to call, e.g. deletecustomer, addcard etc
 * @param {Object} args - operation specific arguments
 *
 * @return {Promise.<TResult>}
 */
CGOperations.prototype.executeOkOrErrorOperation = function(name, args){
    let self = this,
        cg = self.cg();

    self.logger().debug('--> CGOperation::executeOkOrErrorOperation');

    return cg.callOperationPromise(name, args).then(results => {
        return new Promise((resolve, reject) => {
            if ( cg.isError(results[1]) ) {
                reject(new CGError(cg.normalizeError(results[1])));
            } else {
                resolve(cg.isOk(results[1]));
            }
        });
    });
};

/**
 * Card detail object.<br>
 *     This object is typically returned from the operations used for querying card information.
 * <br>
 * Note that the properties <code>macAddress</code>, <code>irdNumber</code> and <code>serialNumber</code>
 * are all optional. If they are present only one of them will be, i.e. the are mutually exclusive.
 *
 * @typedef {Object} CGOperations~CardDetail
 * @property {String} type - The cards type e.g. CryptoGuard
 * @property {string} number - The cards number
 * @property {string} pin - Pin code associated with the card, might be null
 * @property {boolean} isConnected - Flag indicating if this card is connected to anoter card
 * @property {boolean} isExtra - Flag indicating if this card is an extra card
 * @property {boolean} isInvalid - Flag indicating if this card is invalid
 * @property {string} [macAddress] - Associated CPE (device) MAC address
 * @property {string} [irdNumber] - Associated CPE (device) IRD number
 * @property {string} [serialNumber] - Associated CPE (device) Serial number.
 */

const CG_CARD_TO_CARD = [
    ["CardType", "type"],
    ["Number", "number"],
    ["Pin", "pin"],
    ["Connected", "isConnected"],
    ["ExtraCard", "isExtra"],
    ["Invalid", "isInvalid"],
    ["Stored", "isStored"],
    ["MAC", "macAddress"],
    ["IRDNumber", "irdNumber"]
];

const CG_CARD_TO_CARD_MAP = new Map(CG_CARD_TO_CARD);

/**
 * <strong>Get detailed information about a known card</strong><br><br>
 *
 * The returned promise will resolve with an object {@link CGOperations~CardDetail} if there is
 * such a card, otherwise <code>null</code>
 *
 * @param {string} cardNumber - The card to list/get information about.
 */
CGOperations.prototype.listCard = function (cardNumber) {
    let self = this,
        cg = self.cg();
    self.logger().debug('--> CGOperation::listCard');

    let args = {
        cardnumber: cardNumber
    };

    // TODO Generalise and move out
    function toCard(data, map) {
        let result = {};

        for( let [sourceKey, targetKey] of map ) {
            let value = _.get(data, '$.'+ sourceKey);
            if ( !_.isEmpty(value) ) {
                _.set(result, targetKey, value);
            }
        }

        // Convert True/False values in CG to real ones
        for ( let key in result ) {
            if ( key.startsWith('is') ) {
                result[key] = (result[key]==="1")?true:false;
            }
        }

        return result;
    }

    return cg.callOperationPromise("listcard", args).then(results => {
        return new Promise((resolve, reject) => {
            if ( cg.isError(results[1]) ) {
                reject(new CGError(cg.normalizeError(results[1])));
            } else {
                let card = null,
                    parsed = results[1],
                    cardPath = "ListCard.Card";

                if ( _.has(parsed, cardPath) && _.isArray(_.get(parsed, cardPath)) && _.get(parsed, cardPath).length > 0 ) {
                    card = toCard(_.get(parsed, cardPath)[0], CG_CARD_TO_CARD_MAP);
                }
                resolve(card);
            }
        });
    });
};

/**
 * <strong>List details for cards associated with a known customer</strong
 * <br><br>
 *
 * The returned promise will resolve with an <code>array</code> containing zero or
 * more {@link CGOperations~CardDetail} objects.
 *
 * @param {string} customerNumber - The customer number
 * @param {boolean} [excludeStored=false] - Flag indicating if stored or black listed cards is to be excluded or not
 * @param {String|Number} [customerNetworkID] - Customers network ID
 */
CGOperations.prototype.listCustomerCards = function (customerNumber, excludeStored, customerNetworkID) {
    let self = this,
        cg = self.cg();

    self.logger().debug('--> CGOperation::listCustomerCards');

    if ( excludeStored === undefined ) {
        excludeStored = false;
    }

    // TODO Even though the CG call accepts customernetid it ignores it!
    let args = {
        customernumber: customerNumber,
        customernetid: customerNetworkID || self._netid,
        excludestored: (excludeStored)?"1":"0"
    };

    // TODO Generalise and move out, along with the duplicate in listCard!!
    function toCard(data, map) {
        let result = {};

        for( let [sourceKey, targetKey] of map ) {
            let value = _.get(data, '$.'+ sourceKey);
            if ( !_.isEmpty(value) ) {
                _.set(result, targetKey, value);
            }
        }

        // Convert True/False values in CG to real ones
        for ( let key in result ) {
            if ( key.startsWith('is') ) {
                result[key] = (result[key]==="1")?true:false;
            }
        }

        return result;
    }


    return cg.callOperationPromise("listcards", args).then(results => {
        return new Promise((resolve, reject) => {
            if ( cg.isError(results[1]) ) {
                reject(new CGError(cg.normalizeError(results[1])));
            } else {
                let cards = [],
                    parsed = results[1],
                    cardPath = "ListCards.Card";

                if ( _.has(parsed, cardPath) && _.isArray(_.get(parsed, cardPath)) && _.get(parsed, cardPath).length > 0 ) {
                    let arr = _.get(parsed, cardPath);
                    arr.forEach((e, ix) => {
                        cards.push(toCard(e, CG_CARD_TO_CARD_MAP));
                    });

                }
                resolve(cards);
            }
        });
    });
};

/**
 * Add a card to a known/existing customer, e.g. associate an existing card with an
 * existing customer.
 *
 * @param {String} customerNumber - The customer number of the customer to whom the card is to
 *  be added.
 *
 * @param {String} cardNumber - The number of the card to add
 * @param {Object} [opts] - Optional arguments, if specified it MUST only contain one of the specified
 *      attributes.
 * @param {string} [opts.mac] - MAC address of the receiver/CPE
 * @param {string} [opts.irdNumber] - IRD number of the receiver/CPE
 * @param {string} [opts.serialNumber] - Serial number of the receiver/CPE
 *
 * @return {Promise.<TResult>}
 */
CGOperations.prototype.addCard = function (customerNumber, cardNumber, opts) {
    let self = this;
    self.logger().debug("--> CGOperations::addCard");

    const MAP = {
        macAddress: "mac",
        irdNumber: "irdnumber",
        serialNumber: "serialnumber",
    };

    opts = opts || {};

    let args = mapAttributes({}, opts, MAP);
    args.cardnumber = cardNumber;
    args.customernumber = customerNumber;

    return self.executeOkOrErrorOperation("addcard", args);
};

/**
 * Blacklist/Store an existing card.
 *
 * @param {String} cardNumber - Card number of known card to black list/store.
 * @return {Promise.<TResult>}
 */
CGOperations.prototype.blacklistCard = function (cardNumber) {
    let self = this,
        args = {
            cardnumber: cardNumber
        };

    self.logger().debug("--> CGOperations::blacklistCard");

    return self.executeOkOrErrorOperation("blacklistcard", args);
};

/**
 * Connect a card to another card to produce a duplicate/twin/child of that card.
 *
 * @param {String} mainCardNumber - The main cards number
 * @param {String} childCardNumber - The child card, the card to connect
 * @return {Promise.<TResult>}
 */
CGOperations.prototype.connectCard = function (mainCardNumber, childCardNumber) {
    let self = this,
        args = {
            maincard: mainCardNumber,
            connectcard: childCardNumber
        };

    self.logger().debug("--> CGOperations::connectCard");
    return self.executeOkOrErrorOperation("connectcard", args);
};

/**
 * De-/disconnect the specified card from its main/parent card.
 *
 * @param {String} cardNumber - The card number of the card to disconnect.
 * @return {Promise.<TResult>}
 */
CGOperations.prototype.deconnectCard = function (cardNumber) {
    let self = this,
        args = {
            cardnumber: cardNumber
        };

    self.logger().debug("--> CGOperations::deconnectCard");
    return self.executeOkOrErrorOperation("deconnectcard", args);
};


/**
 * Invalidate a known card.
 *
 * @param {String} cardNumber - Card number of card to invalidate
 * @return {Promise.<TResult>}
 */
CGOperations.prototype.invalidateCard = function(cardNumber) {
    let self = this,
        args = {
            cardnumber: cardNumber
        };

    self.logger().debug("--> CGOperations::invalidateCard");
    return self.executeOkOrErrorOperation('invalidatecard', args);
};

/**
 * Revalidate a known and previously invalidated card.
 *
 * @param {String} cardNumber - Number of card to revalidate
 * @return {Promise.<TResult>}
 */
CGOperations.prototype.revalidateCard = function(cardNumber) {
    let self = this,
        args = {
            cardnumber: cardNumber
        };

    self.logger().debug("--> CGOperations::revalidateCard");
    return self.executeOkOrErrorOperation('revalidatecard', args);
};


/**
 * Pair a card with a customer receiver. If pairing is enabled in the SMS...
 *
 * @param {String} cardNumber - The card number to pair
 * @param {Object} [opts] - Optional arguments, if specified it MUST only contain one of the specified
 *      attributes.
 * @param {string} [opts.mac] - MAC address of the receiver/CPE
 * @param {string} [opts.irdNumber] - IRD number of the receiver/CPE
 * @param {string} [opts.serialNumber] - Serial number of the receiver/CPE
 * @return {Promise.<TResult>}
 */
CGOperations.prototype.pairCard = function(cardNumber, opts) {
    let self = this,
        args = {
            cardnumber: cardNumber
        };

    if ( opts ) {
        // Check if opts are up to the precond
        let keys = _.keys(opts);
        if ( keys.length === 1 && ["mac", "irdNumber", "serialNumber"].includes(keys[0])) {
            args[keys[0].toLowerCase()] = opts[keys[0]];
        }
    }

    self.logger().debug("--> CGOperations::pairCard");
    return self.executeOkOrErrorOperation("paircard", args);
};


/**
 * Exchange/Replace a card
 *
 * @param cardNumber - Card to be exchanged, i.e. the old/current card
 * @param newCardNumber - The replacement, i.e. the new card
 */
CGOperations.prototype.exchangeCard = function(cardNumber, newCardNumber) {
    let self = this,
        args = {
            cardnumber: cardNumber,
            newnumber: newCardNumber
        };

    self.logger().debug("--> CGOperations::exchangeCard");
    return self.executeOkOrErrorOperation("exchangecard", args);
};

/**
 * Unpair a previously paired card from a customer receiver.
 *
 * @param {String} cardNumber - The card number of the card to unpair
 */
CGOperations.prototype.unpairCard = function(cardNumber) {
    let self = this,
        args = {
            cardnumber: cardNumber
        };

    self.logger().debug("--> CGOperations::unpairCard");
    return self.executeOkOrErrorOperation("unpaircard", args);
};

/**
 * UTIL: Converts the specified date to a date string on
 * the form YYYY-MM-DD if it is of type Date, otherwise just
 * return it as is.
 *
 * @param {Date|*} date - The date
 */
function toDateStr(date) {

    // TODO momentjs is available use that instead!
    if ( _.isDate(date) ) {
        return date.toISOString().substr(0, 10);
    }
    return date;
}

/**
 * Add a subscription (channel package) to an active end existing card.
 *
 * @param cardNumber - The number of the card to which the subscription is to be added.
 * @param articleNumber - Article number for the subscription to add.
 * @param {Date|String} [startDate] - Start date on the form YYYY-MM-DD if it is a string
 * @param {Date|String} [endDate] - End date on the form YYYY-MM-DD if it is a string
 * @return {Promise.<TResult>}
 */
CGOperations.prototype.addSubscription = function (cardNumber, articleNumber, startDate, endDate){
    let self = this,
        args = {
            cardnumber: cardNumber,
            artnr: articleNumber
        };

    if ( startDate ) {
        args.startdate = toDateStr(startDate);
    }

    if ( endDate ) {
        args.enddate = toDateStr(endDate);
    }

    self.logger().debug("--> CGOperations::addSubscription");
    return self.executeOkOrErrorOperation("addsubscription", args);
};

/**
 *
 * @param {String} cardNumber - The card number wher the subscription is assigned/added
 * @param {String|Number} articleNumber - Subscription article number
 * @param {Date|String} [stopDate] - Stop date
 * @return {Promise.<TResult>}
 */
CGOperations.prototype.stopSubscription = function (cardNumber, articleNumber, stopDate) {
    let self = this,
        args = {
            cardnumber: cardNumber,
            artnr: articleNumber
        };

    if ( stopDate ) {
        args.stopdate = toDateStr(stopDate);
    }

    self.logger().debug("--> CGOperations::stopSubscription");
    return self.executeOkOrErrorOperation("stopsubscription", args);
};


/**
 * <strong>SubscriptionInfo</strong><br><br>
 * Object describing a subscription.
 *
 * ```xml
 *  <?xml version="1.0" encoding="UTF-8" ?>
 * <ListSubscription>
 *      <APIVersion>1.26</APIVersion>
 *      <Subscription ArtNr="1"
 *          OrgStartDate="2011-09-20"
 *          ExpireDate="2012-09-20"
 *          EndDate="2012-09-20"
 *          Name="Adminpaket"
 *          InvoiceName=”Adminpaket”
 *          InitDisPeriod=“12”
 *          DisPeriod=“1” />
 *      <PPVSubscription
 *          PPVProgramID="1"
 *          StartTime="2012-09-20 18:00:00"
 *          StopTime="2012-09-20 19:00:00"
 *          ProgramName="Adminpaket"
 *          InitDisPeriod=“24”
 *          DisPeriod=“3” />
 *      <Status>OK</Status>
 * </ListSubscription>
 * ```
 *
 * @typedef {Object} CGOperations~SubscriptionInfo
 * @property {String} articleNumber - The article number for the subscription
 * @property {String} name - The name of the subscription
 * @property {String} type - Subscription type, i.e. PPV or <code>null</code>
 */


/**
 * List subscriptions associated with the specified card.
 *
 * @param {String} cardNumber - The card number
 */
CGOperations.prototype.listSubscriptions = function (cardNumber) {
    let self = this,
        cg = self.cg(),
        args = {
            cardnumber: cardNumber
        };

    function toSubscription(e) {
        return {
            articleNumber: e.$.ArtNr,
            name: e.$.Name,
            type: null
        };
    }

    self.logger().debug("--> CGOperations::listSubscriptions");

    return cg.callOperationPromise("listsubscription", args)
        .then(results => {
            return new Promise((resolve, reject) => {
                if (cg.isError(results[1])) {
                    reject(new CGError(cg.normalizeError(results[1])));
                } else {
                    let response = results[1];

                    if (_.has(response, "ListSubscription") &&
                        (_.has(response, "ListSubscription.Subscription") || _.has(response, "ListSubscription.PPVSubscription"))) {

                        let allSubscriptions = ([].concat(_.get(response, "ListSubscription.Subscription", [])))
                            .concat(_.get(response, "ListSubscription.PPVSubscription", []));

                        allSubscriptions = allSubscriptions.map(toSubscription);
                        resolve(allSubscriptions);
                    } else {
                        resolve(false);
                    }
                }
            });
        });
};

/**
 * List available networks over at the configured Cryptoguard SMS server.
 *
 * The promise will resolve with an array of objects representing the networks if
 * those a successfully retrieved from the CG SMS server.
 *
 * @example
 *
 * // cgo is reference to a CGOperations object
 * cgo.listNetworks()
 *  .then(result => {
 *      result.forEach((e) => {
 *          console.log('id ' + e.id + ' name: ' + e.name);
 *      });
 *  })
 *  .catch(err => {
 *      // Handle error...
 *  });
 *
 * @return {Promise.<TResult>}
 */
CGOperations.prototype.listNetworks = function() {
    let self = this,
        cg = this.cg();

    self.logger().debug("--> CGOperations::listNetworks");

    return cg.callOperationPromise("listnetworks").then(results => {
        return new Promise((resolve, reject) => {
            if ( cg.isError(results[1]) ) {
                reject(new CGError(cg.normalizeError(results[1])));
            } else {

                let networks = [],
                    parsedBody = results[1];

                if ( _.has(parsedBody, "NetworkList.Network") && _.isArray(parsedBody.NetworkList.Network) ) {
                    let arr = parsedBody.NetworkList.Network;

                    _.each(arr, (e)=>{
                        networks.push({name: e.$.Name, id: e.$.NetId});
                    });
                }

                resolve(networks);
            }
        });
    });
};

/**
 * <strong>CustomerInfo</strong object<br><br>
 * Object describing a customer.
 *
 * @typedef {Object} CGOperations~CustomerInfo
 *
 * @property {String|Number} customerNumber - A customer unique identifier
 * @property {String} name - The first and last name of the customer.
 * @proeprty {String|Number} netId - The network id where the customer belongs
 * @property {String} sseqNo - Customers social security number, usually the swedish variant of it, i.e. personnumer.
 * @property {Object} [address] - Customer address object
 * @property {String} [address.street] - Customers street address, e.g. Cul de sac 123
 * @property {String} [address.co] - Customers C/O name/address if applicable
 * @property {String} [address.postalCode] - Customers address postal code, e.g. 123 45
 * @property {String} [address.city] - Customer address city, e.g. Big City
 * @property {Object} [contact] - Customer contact details
 * @property {String} [contact.phone] - Customers (landline) phone number
 * @property {String} [contact.mobile] - Customers mobile phone number
 * @property {String} [contact.mail] - Customers mail address
 * @property {Object} [invoiceAddress] - Optional invoice address if other then customers address
 * @property {String} [invoiceAddress.name] - Name of the person or part receiving the invoice
 * @property {String} [invoiceAddress.address] - Billing address, i.e. street address
 * @property {String} [invoiceAddress.coAddress] - Billing C/O address
 * @property {String} [invoiceAddress.postalCode] - Billing postal code
 * @property {String} [invoiceAddress.city] - Billing city
 *
 */

/**
 * Check if a customer exists in the CG (SMS) system having the specified
 * <code>customerNumber</code> and that is associated with the specified <code>customerNetID</code>,
 * or if it is not specified, the configured network ID for this <code>CGOperations</code> object.
 *
 * @param {String} customerNumber - The customers customer number
 * @param {String} customerNetID - Optional customer network ID, if not specified the
 *  configured network ID will be used.
 *
 * @return {Promise.<TResult>}
 */
CGOperations.prototype.customerExists = function(customerNumber, customerNetID) {
    let self = this;

    self.logger().debug('--> CGOperation::customerExists');
    return self.getCustomer(customerNumber, customerNetID).then((result) => {
        return Promise.resolve((result===false)?false:true);
    });
};

/**
 * Get a customer by his/hers unique key <code>customerNumber</code> (probably)<br><br>
 *
 * @param {String|Number} customerNumber - The customer number
 * @param {String} customerNetID - Optional customer network ID, if not specified the
 *  configured network ID will be used.
 */
CGOperations.prototype.getCustomer = function(customerNumber, customerNetID) {
    let self = this;

    self.logger().debug('--> CGOperation::getCustomer');

    customerNetID = customerNetID || self._netid;
    let cg = self.cg(),
        args = {
            search: customerNumber,
            netid: customerNetID
        };

    function findElementWith(arr, property, value) {
        let result = -1;
        arr.forEach(function(e, ix) {
            if ( _.has(e.$, property) && e.$[property] == value ) {
                result = ix;
                return;
            }
        });
        return result;
    }

    /**
     * Convert a customer result as returned from CG to a CGOperations customer.
     * All properties in the CG response that are empty are ignored.
     *
     * @param {Object} data - Data object representing the customer. This element is what
     *  the XML parser (XML2JS) uses to represent a XML element in, attributes in data.$
     *
     * @return {{customerNumber: *, netId: *}}
     */
    function toCustomerInfo(data) {

        let result = {};

        for( let [sourceKey, targetKey] of CG_CUSTOMER_TO_CUSTOMER_MAP ) {
            let value = _.get(data, '$.'+ sourceKey);
            if ( !_.isEmpty(value) ) {
                _.set(result, targetKey, value);
            }
        }

        return result;
    }

    return cg.callOperationPromise('searchcustomers', args).then((results) => {
        let result = null;
        if ( results ) {
            let response = results[1];
            if ( _.has(response, "SearchCustomers") && _.has(response, "SearchCustomers.Customer") ) {
                let searchResult = response.SearchCustomers.Customer;
                if ( _.isArray(searchResult) ) {
                    let elementIx = findElementWith(searchResult, "CustomerNumber", customerNumber);
                    if ( elementIx >= 0 ) {
                        result = Promise.resolve(toCustomerInfo(searchResult[elementIx]));
                    }
                }
            }
        }
        if ( result === null ) {
            result = Promise.resolve(false);
        }
        return result;
    });
};

/**
 * Add a customer to the CG (SMS) system. The operation returns a promise
 * that will either resolve or be rejected.
 *
 * The following customer information can be included:
 *
 *  customerNumber  - Unique (?) customer identifier
 *  name            - Customer name, usually first and last name, in that order.
 *  sseqNo          - Customers Social Security number, i.e. swedish personnummer.
 *  address         - Address, i.e. street
 *  coAddress       - Care of Address
 *  postalCode      - Postal code
 *  city            - City
 *  phone           - Land line phone number
 *  mobile          - Mobile phone number
 *  mail            - Mail address
 *  billing         - Billing/Invoice address
 *
 *      name        - Name
 *      address     - Address
 *      coAddress   - Care of Address
 *      postalCode  - Postal code
 *      city        - City
 *
 *
 * <strong>NOTE</strong> that <code>customerNumber</code> is the only mandatory piece of information
 * that is needed about a customer.
 *
 * If successfully added the returned promise will be resolved with the customers customer number
 * and the net id for the customer, e.g.:
 *
 * {
 *   "customerNumber": "12345",
 *   "netId": "12"
 * }
 *
 * @param {CGOperations~CustomerInfo} customerInfo - Customer information object.
 * @param {Number|String} [customerNetID] - Optional net ID, overriding the configured net ID.
 *
 * @return {Promise.<TResult>}
 */
CGOperations.prototype.addCustomer = function(customerInfo, customerNetID) {
    let self = this;

    self.logger().debug("--> CGOperations::addCustomer");

    customerInfo = customerInfo || {};
    customerNetID = customerNetID || self._netid;

    let args = mapAttributes({}, customerInfo, CUSTOMER_ATTRIBUTE_MAP);
    args.customernetid = customerNetID;

    let cg = self.cg();

    return cg.callOperationPromise("addcustomer", args).then((results) => {
        return new Promise((resolve, reject) => {
            if ( cg.isError(results[1]) ) {
                reject(new CGError(cg.normalizeError(results[1])));
            } else {
                let result = {
                        netId: customerNetID
                    },
                    parsedBody = results[1];
                if ( _.has(parsedBody, 'AddCustomer.CustomerNumber') && _.isArray(parsedBody.AddCustomer.CustomerNumber) ){
                    result.customerNumber = parsedBody.AddCustomer.CustomerNumber[0];
                }
                resolve(result);
            }
        });
    });

};

/**
 * Delete an existing customer.<br><br>
 *
 * <strong>Note</code>: this has implications!! See Cryptoguards documentation regarding deletion of customers.
 *
 * @param customerNumber - Customer number
 * @param {Number|String} [customerNetID] - Optional customer network ID,
 *  overriding the configured net ID.
 *
 * @return {Promise.<TResult>}
 */
CGOperations.prototype.deleteCustomer = function(customerNumber, customerNetID) {

    let self = this,
        args = {
            customernumber: customerNumber,
            customernetid: customerNetID || self._netid
        };

    self.logger().debug("--> CGOperations::deleteCustomer");
    return self.executeOkOrErrorOperation("deletecustomer", args);
};

/**
 * The CGOperations (prototype) class?
 * @type {CGOperations}
 */
module.exports = CGOperations;
