/**
 *
 * @module cg-client
 * @author H Svalin (PE)
 */
"use strict";

// Version Information for module
const VERSION_INFORMATION = "0.0.4";

const
    createDefaultLogger = require('./utils').createDefaultLogger,
    CGOperations = require('./cg-operations');


/**
 * A better alias/name for this object/prototype should be CGCustomerClient
 *
 * @param {Object} opts - Configuration options
 * @param {String} opts.customerNumber - The customer number
 * @param {String} opts.netId - Network ID
 * @param {Object} opts.cg - CG specific configuration options
 * @param {String} opts.cg.baseUrl - CG base URL, e.g
 * @param {String} opts.cg.token - CG API Token
 *
 * @constructor
 */
function CGClient(opts, logger) {

    // TODO PreCond check opts
    this._options = Object.assign({}, opts);

    this._customerDetails = null;

    if ( logger ) {
        this._logger = logger;
    } else {
        this._logger = createDefaultLogger('CGClient', 'debug');
    }
};

/**
 * Return the configured (winston) logger
 * @return {*}
 */
CGClient.prototype.logger = function () {
    return this._logger;
};

/**
 * Returns customer details if available. If this returns
 * <code>null</code> try <code>refresh()</code>.
 *
 */
/**
 *
 * @return {Promise.<TResult>}
 */
CGClient.prototype.details = function() {

    this.logger().debug("--> CGClient::details");

    let result = null;
    if ( !this._customerDetails ) {
        result = this.operations().getCustomer(this._options.customerNumber, this._options.netId);
    } else {
        result = Promise.resolve(this._customerDetails);
    }

    // This should work (with this) according to the semantics of arrow functions...
    result.then(result => {
        this._customerDetails = result;
        return result;
    });

    return result;
};

/**
 * Returns the customers all cards
 *
 * @return {Promise.<TResult>}
 */
CGClient.prototype.cards = function() {

    this.logger().debug("--> CGClient::cards");
    let result = null;

    if ( this._cards ) {
        result = Promise.resolve(this._cards);
    } else {
        result = this.operations().listCustomerCards(this._options.customerNumber)
            .then(result => {
                this._cards = result;
                return result;
            });
    }
    return result;
};

/**
 *
 */
CGClient.prototype.operations = function () {

    let result = null;
    if ( this._cg_operations ) {
        result = this._cg_operations;
    } else {
        let opts = {
            baseUrl: this._options.cg.baseUrl,
            token: this._options.cg.token,
            netid: this._options.netId
        };
        result = new CGOperations(opts, this.logger());
    }
    return result;
};

/**
 * This will load/update the attributes of this CG client by retrieving them over
 * at the configured CG server.
 *
 * @return {Promise.<TResult>}
 */
CGClient.prototype.refresh = function () {

    // TODO Implement refresh
    this.logger().debug('--> CGClient::refresh');
    let self = this;

    return Promise.all([
        self.details(),
        self.cards()
    ]).then(results => {
       return true;
    });
};


/**
 * Replace a customers card with a new one. This operation will
 * transfer all subscriptions and hardware assignment from one card
 * to another if the card, the one being replaced is a main card.
 *
 *      * What is a main card, a card that isn't connected?
 *
 * If the card is a connected card, i.e. a twin of an existing card, the
 * current card will be disconnected and the new connected
 *
 *      * Is that enough?
 *
 * @param {Object} args
 * @param {String} args.cardNumber - The card number to remove/store
 * @param {String} args.newCardNumber - The new card number
 */
CGClient.prototype.replaceCard = function (args) {

};

/**
 *
 * @param {Object} args - Arguments object
 * @param {String} args.cardNumber - The card number
 * @param {String} args.hardwareIdentifier - The hardware identifier, i.e. mac, IRD or serial number
 * @param {String} args.newHardwareIdentifier - The new hardware identifier
 */
CGClient.prototype.replaceHardware = function (args){

};


/**
 *
 * @param {Object} args
 * @param {String} args.cardNumber - The card number to remove/store
 * @param {String} args.newCardNumber - The new card number
 * @param {String} args.hardwareIdentifier - The hardware identifier, i.e. mac, IRD or serial number
 * @param {String} args.newHardwareIdentifier - The new hardware identifier
 *
 */
CGClient.prototype.updateCardAndHardware = function (args) {

};

// Export the ?class?
module.exports = CGClient;

