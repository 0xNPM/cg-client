/**
 * CG-Facade module.
 *
 * @module lib/cg-facade
 * @author H Svalin
 */
"use strict";

const winston = require('winston');
const xml2js = require('xml2js');
const xml = require('xml');
const request = require('request');

const Parser = xml2js.Parser;
const _ = require('lodash');

// Module version number/information string
const VERSION_INFORMATION = "0.0.4";

/**
 * <strong>Extract and return the status string value from a parsed CG XML response</strong>
 * <br><br>
 *
 * A typical XML response looks, if it was a success, like this:
 *
 * ```xml
 * <NetworkList>
 *   <APIVersion></APIVersion>
 *   <Network Name="nisse" NetId="1"/>
 *   ...
 *   <Status>OK</Status>
 * </NetworkList>
 * ```
 * If it is a failed response, i.e. some error occurred over at CG due to the client submitting
 * junk, or for some other reason, the XML response will look like the following:
 *
 * ```xml
 * <SearchCustomers>
 *   <APIVersion>API Version 1.27</APIVersion>
 *   <ErrorNumber>14</ErrorNumber>
 *   <ErrorMsg>Network does not exist </ErrorMsg>
 *   <Status>ERROR</Status>`
 * </SearchCustomers>
 * ```
 *
 * This function will focus on the Status element, i.e. extract and return the text child of it.
 * Typical status values are: OK, ERROR ...
 *
 * @param {Object} parsedBody - A response received from the CG facade
 * @returns {string|null} The status value
 * @private
 */
function cg_response_status_value(parsedBody) {
    let result = null;
    for ( let k in parsedBody ) {
        if ( _.has(parsedBody[k], "Status") ) {
            result = parsedBody[k].Status[0];
            break;
        }
    }
    return result;
}

/**
 * <strong>Test if a response status value returned by CG is a certain value</strong>
 * <br><br>
 *
 * @param {string} value - The value to test for
 * @param {Object} parsedBody - The parsed CG response XML body.
 * @returns {boolean} If the parsed body has the specified value <code>true</code>, <code>false</code>
 *      otherwise
 *
 * @private
 */
function cg_response_status_value_is(value, parsedBody) {

    let cValue = cg_response_status_value(parsedBody);
    if ( cValue ) {
        // TODO Do we trust CG to always return correct upper cased value?
        return (""+value).toUpperCase() === cValue.toUpperCase();
    }
    return false;
}

/**
 * <strong>Is the response status OK</strong>
 *
 * @function
 * @param {Object} parsedBody - A parsed CG response body
 * @return {boolean}
 * @private
 */
const cg_response_status_is_ok = cg_response_status_value_is.bind(undefined, "OK");

/**
 * <strong>Is the response status ERROR</strong>
 *
 * @function
 * @param {Object} parsedBody - A parsed CG response body
 * @return {boolean}
 * @private
 */
const cg_response_status_is_error = cg_response_status_value_is.bind(undefined, "ERROR");

/**
 * <strong>InOutTrace object</strong>
 *
 * This type of object is usually returned by the {@link create_in_out_trace} function.
 *
 * @typedef {Object} InOutTrace
 * @property {Object} in - Incoming, i.e. response, trace
 * @property {number} in.statusCode - The status code
 * @property {Object} in.headers - The headers
 * @property {string} in.body - The body of the response if present and applicable
 *  
 * @property {Object} out - Outgoing, i.e.  request, trace
 * @property {string} out.method - The method, i.e. HEAD, GET etc.
 * @property {string} out.host - Name of host
 * @property {string} out.path - The path
 * @property {Object} out.headers - The headers
 * @property {string} [out.body] - The body of the request if applicable
 */

/**
 * <strong>Create and return an In/Out trace object for a HTTP request/response</strong>
 *
 * <br>
 *     A typical resulting object would be:
 *
 * ```js
 * {
 *   out: {
 *      method: "GET",
 *      host: "some.host.com",
 *      path: "/api/v1/customer,
 *      headers: {}
 *      body: ""
 *   },
 *   in: {
 *      statusCode: 200,
 *      headers: {}
 *   }
 * }
 * ```
 *
 * @param {IncomingMessage} response - A response returned from <code>request</code>
 * @param {boolean} [includeBodyIn=true] - Include the incoming body in the trace
 * @param {boolean} [includeBodyOut=true] - Include the outgoing body, if applicable, in
 *  the trace
 * @returns {InOutTrace} an In Out trace object
 * @private
 */
function create_in_out_trace(response, includeBodyIn=true, includeBodyOut=true) {

    let result = {
        out: {
            method: response.request.method,
            host: response.request.host,
            path: response.request.path,
            headers: Object.assign({}, response.request.headers)
        },
        in: {
            statusCode: response.statusCode,
            headers: Object.assign({}, response.headers)
        }
    };

    if ( includeBodyIn ) {
        result.in.body = response.body;
    }

    if ( includeBodyOut ) {
        result.out.body = response.request.body;
    }

    return result;
}

/**
 * <strong>Creates a Cryptoguard (facade) object that simplifies calling the Cryptoguards API
 * endpoint</strong><br><br>
 *
 * The returned Cryptoguard facade object is bound to a configured CG API server, ... TBD
 *
 * @param {Object} options - Configuration options
 * @param {string} [options.baseUrl=http://demo.cryptoguard.se] - Base URL to Cryptoguards server
 * @param {string} options.token - API access token
 * @param {string} [options.userAgent=CG-Client V_1.0.0] - User agent header value to use when sending requests to CG.
 * @param {number} [options.timeout=60] - Number of seconds we will wait for CG to produce a response.
 * @param {boolean} [options.trace=false] - Flag indicating if requests and responses to CG should be returned in
 *  callbacks and/or Promises
 *
 * @param {Winston} logger - Optional (Winston) logger
 *
 * @returns {{version: *, options: *}}
 */
const cryptoguard_facade_factory = function (options, logger){

    const DEFAULT_CALL_OPTIONS = {
        method: "no-method-name",
        args: null

    };

    var _options = {},
        _default_options = {
            baseUrl: "http://demo.cryptoguard.se",
            endpoint: "/api.php",
            token: null,
            userAgent: 'CG-Client V_' + VERSION_INFORMATION,
            timeout: 60,
            trace: false
        },
        _logger;

    var _create_xml_command,
        _parse_response,
        _ping_cryptoguard,
        pingPromise,
        _invoke_cg,
        _call_operation,
        _call_operation_promise,
        _xml_parser = null,
        _get_xml_parser,
        _get_timeout_promise,
        _promisify,
        _is_cg_error,
        _is_cg_ok,
        _normalize_error;


    /**
     * Tests a parsed XML response from CG to see if it is an ERROR
     *
     * @param {Object} parsedBody - A parsed XML body
     * @return {boolean}
     * @private
     */
    _is_cg_error = function (parsedBody) {
        return cg_response_status_is_error(parsedBody);
    };

    /**
     * Tests a parsed XML response from CG to see if it is OK.
     *
     * @param {Object} parsedBody - A parsed XML body
     * @return {boolean}
     * @private
     */
    _is_cg_ok = function (parsedBody) {
        return cg_response_status_is_ok(parsedBody);
    };

    /**
     * Returns a normalized CG error response. For example this:
     *
     *      <SearchCustomers>
     *          <APIVersion>API Version 1.27</APIVersion>
     *          <ErrorNumber>14</ErrorNumber>
     *          <ErrorMsg>Network does not exist </ErrorMsg>
     *          <Status>ERROR</Status>
     *      </SearchCustomers>
     *
     *
     * will be converted into this:
     *
     *      {
     *          rootElement: "SearchCustomer",
     *          errorMessage: "",
     *          errorNumber: "12",
     *          status: "ERROR" <-- Tautological I know...
     *          APIVersion: "API Version 1.27"
     *      }
     *
     * @param {Object} parsedBody - The parsed XML response from CG
     * @return {CGErrorResponse} - The CGErrorResponse error object
     * @private
     */
    _normalize_error = function (parsedBody){
        if ( !cg_response_status_is_error(parsedBody) ) {
            return null;
        }

        let keys = _.keys(parsedBody);
        let rootName = keys[0];

        return {
            rootElement: rootName,
            errorMessage: parsedBody[rootName].ErrorMsg[0],
            errorNumber: parsedBody[rootName].ErrorNumber[0],
            status: parsedBody[rootName].Status[0],
            APIVersion: parsedBody[rootName].APIVersion[0]
        };
    };

    /**
     * UTIL: Create and return a timeout promise that will reject after
     * a certain amount of time.
     *
     * @param {Number} [timeout] - Timeout in seconds, or fractions of seconds. If not specified
     *  the configured timeout <code>options.timeout</code> @see {@link cryptoguard_facade_factory}, will be used.
     *
     * @returns {Promise} A promise that will reject after the specified timeout.
     * @private
     */
    _get_timeout_promise = function(timeout) {
        timeout = timeout || _options.timeout;
        timeout = Math.floor(timeout*1000);

        return new Promise((resolve, reject) => {
            setTimeout(() => {
                reject(new Error(`CG Facade, forced timed out after (${timeout}) seconds`));
            }, timeout);
        });
    };

    /**
     * UTIL: Lazy evaluation of XML parser used by this object.
     *
     * @returns {Parser} A parser instance
     * @private
     */
    _get_xml_parser = function() {
        if ( _xml_parser === null ) {
            _xml_parser = new Parser();
        }
        return _xml_parser;
    };

    /**
     * UTIL: Internal use mostly
     *
     * Creates a crypto guard API specific XML command message string based on the
     * specified method (name) and optional attributes. The resulting command will
     * have the configured token added to it by default.
     *
     * @example
     *
     * let attrs = {
     *  network: "300",
     *  name: "Dude"
     * };
     *
     * let command = _create_xml_command('dosome', attrs);
     * console.log(command); // <xmldata method="dosome" token="XYZ123..." network="300" name="dude"/>
     *
     * @param {String} method - Name of method, i.e. the method attribute in the resulting XML message
     * @param {Object} attributes - Optional XML command string (element) attributes
     * @param {String} [token] - Optional CG access token if the configured token is to be overriden.
     *
     * @return {String} The XML string representing the command
     * @private
     */
    _create_xml_command = function(method, attributes, token) {

        let attrs = {
            method: method,
            token: token || _options.token
        };

        if ( attributes ) {
            Object.assign(attrs, attributes);
        }

        let message = {};
        message.xmldata = [{_attr: attrs}, ""];

        return xml(message);
    };

    /**
     * Invoke CGs XML RPC:ish endpoint by POSTing an XML command to it.
     *
     * @param options - Options used when calling CG
     * @param {invokeCallback} callback - Callback called when done
     * @private
     */
    _invoke_cg = function(options, callback) {

        options = options || {};
        options = Object.assign({}, DEFAULT_CALL_OPTIONS, options);

        let request_opts = {
            url: _options.baseUrl + _options.endpoint,
            form: {
                xml: _create_xml_command(options.method, options.args)
            },
            headers: {
                "User-Agent": _options.userAgent
            }
        };

        _logger.debug(`POST request to path: ${request_opts.url}, body: ${request_opts.form.xml} `);

        request.post(request_opts, (err, response, body) => {
            if ( err ) {
                _logger.error('CG facade: POST request error: ' + err.toString());
                return callback(err);
            } else {
                let cbValues = [null, response];
                if ( response.statusCode === 200 && body ) {
                    _get_xml_parser().parseString(body, (err, parsedBody) => {
                        if ( err ) {
                            _logger.warn("CG facade: " + err.toString());
                        }
                        cbValues.push(parsedBody);
                    });
                }

                if ( _options.trace ) {
                    cbValues.push(create_in_out_trace(response));
                }

                callback(...cbValues);
            }
        });
    };

    /**
     * Invoke the specified method on the CryptoGuard (CG) API. If an error occurs
     * when performing the (HTTP POST) request to CGs API, the callback will be invoked with
     * the error. This is usually because of some networking error.
     *
     * Upon success, i.e. the CG endpoint has returned a response, even though it might be a 4XX or 5XX,
     * the callback will be called with error set to null and a response object, where the response object has
     * the following properties assigned:
     *
     * @param {String} method - Name of method to call, e.g. 'addcustomer', 'deletecustomer' etc.
     * @param {Object} parameters - Optional parameters to include in the call
     * @param {invokeCallback} callback - Callback for handling the invocation response
     *
     * @private
     */
    _call_operation = function(method, parameters, callback) {

        if ( typeof parameters === 'function' ) {
            callback = parameters;
        }

        let params =  null;
        if ( typeof parameters === 'object' ) {
            params = parameters;
        }


        let opts = {
            method: method,
            args: params
        };

        _invoke_cg(opts, callback);
    };

    /**
     *
     * Returns a promise that will resolve with an array containing the
     * response as the first element and an optional parsed body, i.e.
     * the XML returned from CG parsed using <code>xml2js.Parser</code>,
     * as the second element.
     *
     * If this object was created with the option
     * trace set to <code>true</code> then a third element will be included in
     * the resolved array. This element contains a request response trace of
     * the HTTP request made to CG.
     *
     * @param {String} method - Name of CG method to call
     * @param {Object} parameters - Parameters to include in the call
     * @param {Number} [timeout] - Timeout in seconds
     * @returns {Promise.<*>}
     *
     * @private
     */
    _call_operation_promise = function (method, parameters, timeout) {

        let invokePromise = new Promise((resolve, reject) => {
            _call_operation(method, parameters, (err, response, parsedBody, trace) => {
                if ( err ) {
                    reject(err);
                } else {
                    let result = [response, parsedBody];
                    if ( trace ) {
                        result.push(trace);
                    }
                    resolve(result);
                }
            });
        });

        return Promise.race([_get_timeout_promise(timeout), invokePromise]);
    };

    /**
     * Ping the configured CG to see if it is reachable.
     *
     * @param {pingCallback} callback - The callback
     * @private
     */
    _ping_cryptoguard = function(callback) {
        _call_operation('listnetworks', (err, resp, parsed)=>{
            if ( err ) {
                return callback(err);
            }

            if ( parsed && _.has(parsed, 'NetworkList') ) {
                let result = parsed.NetworkList.APIVersion[0];
                callback(null, result);
            } else {
                callback(new Error("Unexpected error when calling CG"));
            }
        });
    };

    /**
     * <strong>Ping the configured CG to see if it is reachable</strong><br><br>
     *
     *     If the returned promise resolves, the CG server is reachable and its
     * version information string is passed as the argument.<br><br>
     *
     * If CG cannot be reached due to some error, or if there is a
     * timeout the promise will be rejected with an <code>Error</code>.
     *
     * @param {number} [timeout=60] - Timeout in seconds until we give up waiting for
     *  a response from CG.
     *
     * @returns {Promise.<*>}
     */
    pingPromise = function(timeout){

        let pingPromise = new Promise((resolve, reject) => {
           _ping_cryptoguard((err, result) => {
               if ( err ) {
                   reject(err);
               } else {
                   resolve(result);
               }
           });
        });

        return Promise.race([_get_timeout_promise(timeout), pingPromise]);
    };

    // Validate options TODO

    // Initialize options, merge in _default_options if necessary
    _options = options || _default_options;
    if (options) {
        _options = Object.assign({}, _default_options, options);
    }

    if ( !logger ) {
        _logger = new winston.Logger({
            level: "debug",
            transports: [
                new (winston.transports.Console)()
            ]
        });
    } else {
        _logger = logger;
    }

    var _api = {
        options: function () {
            return _options;
        },
        ping: _ping_cryptoguard,
        pingPromise: pingPromise,
        callOperation: _call_operation,
        callOperationPromise: _call_operation_promise,
        isOk: _is_cg_ok,
        isError: _is_cg_error,
        normalizeError: _normalize_error
    };

    return _api;
};

/**
 * Factory function that will create a CG Facade object.
 * @type {cryptoguard_facade_factory}
 */
module.exports = cryptoguard_facade_factory;

/**
 * Ping invocation callback that will be called when the ping call
 * is done. If an error occurred, i.e. networking errors, the first
 * argument in the callback will be the Error object. If successfully
 * pinged <code>err</code> will be <code>null</code> and <code>response</code>
 * will be a string containing Cryptoguard APIs version string, e.g. API Version 1.27.
 *
 * @callback pingCallback
 *
 * @param {Error} err - The error if any occurred
 * @param {String} [response] - The CG Version information string
 */

/**
 * Invocation callback will be called when a <code>callOperation</code> call is
 * done.
 *
 * @callback invokeCallback
 *
 * @param {Error} err - The error if any occurred
 * @param {IncomingMessage} [response] - The invocation response
 * @param {Object} [parsedBody] - The parsed (XML) body
 */

/**
 * <strong>Object representing a CG error XML payload</strong>
 *
 * @typedef {Object} CGErrorResponse
 * @property {String} rootElement - The root elements name of the returned payload containing the error.
 * @property {string} errorMessage - The error message returned from by CG
 * @property {string|number} errorNumber - Error number returned from CG
 * @property {string} status - The status returned from CG, e.g. either "ERROR" or "OK"
 * @property {string} APIVersion - API version string returned from CG
 */

if (require.main === module) {

    let xml = "<SearchCustomers><APIVersion>API Version 1.27</APIVersion><ErrorNumber>14</ErrorNumber><ErrorMsg>Network does not exist </ErrorMsg><Status>ERROR</Status></SearchCustomers>";
    let parser = new Parser();

    parser.parseString(xml, (err, pbody)=>{
        let r = normalized_cg_error(pbody);
        console.log(r);
    })


}