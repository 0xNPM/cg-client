# CG Client

> Simple and Versatile(?) Cryptoguard API Client

**!! WORK IN PROGRESS !!**

## Releases

### V_0.0.1 - V_0.0.3

> Before releases 

### V_0.0.4

> Major features in this release

* All [CG Operations](#CGOperations) realised

## TODO

>Orchestrated interactions with CG

- [ ] **CG Business** (Currently named CGClient)
    - [X] Read/Get Customer information
    - [X] Read/Get Customers associated cards
    - [ ] Replace card
    - [ ] Replace hardware
    - [ ] Replace both hardware and card
    - [ ] Add base TV subscription
    - [ ] Add addon TV subscription
    - [ ] Add subscription (channel package)
    - [ ] *what more...*


>One to one mapping of CG commands to operations

- [ ] **CG Operations**
    - [X] Customer Exists
    - [X] Get customer by customer number (ID)
    - [X] Add Customer
    - [X] Delete/Remove Customer (and all associated resources for that customer)
    - [X] List Card (i.e. get card details)
    - [X] List a customers all cards
    - [X] Add Card
    - [X] Add Card with Hardware associated, i.e. IRD number (not the other kinds, mac etc) 
    - [X] Connect Card
    - [X] Disconnect Card
    - [X] Blacklist Card
    - [X] Invalidate Card
    - [X] Revalidate Card
    - [X] Exchange/Replace card
    - [X] Add subscription
    - [X] Add subscription with start and end dates
    - [X] List subscriptions associated with a known card
    - [X] Stop subscritpion
    - [X] Stop subscription with stop date
    - [X] Pair Card to a device (STB) using IRD
    - [X] Unpair Card from a device (STB)

> Basic HTTP (POST) interactions with CG

- [X] **CG Facade**
    - [X] Ping functionality w callback and promise
    - [X] Basic method call w callback
    - [X] Basic method call w promise
    - [X] CG Request diagnostics tracing/collecting
    
> Module/Project related

- [ ] JSDoc
    - [ ] Embedded examples
    
## Getting Started
    
> TBD    
    
```javascript
const CGOperations = require('cg-client'),
      options = {
        baseUrl: "http://demo.cryptoguard.se",
        token: "your-secret-token",
        netid: "20"
      };

let cgOperations = new CGOperations(options);

// Add a new customer
let customer = {
    customerNumber: "XYZ-1234",
    name: "Nomen Nescius"
};

cgOperations.addCustomer(customer)
    .then(result => {
        // result is an object
        console.log("Added a customer with customerNumber: " + result.customerNumber);
    })
    .catch(err => {
        console.log("Failed to add customer: ", err);
    });

```    